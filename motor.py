from unittest.mock import Mock
import time

def sleep(*args,**kwargs):
    print(args,kwargs)
    time.sleep(5)

largemotor = Mock(name='LargeMotor')
LargeMotor = Mock(return_value=largemotor)

OUTPUT_B = Mock(name='OUTPUT_B')

OUTPUT_C = Mock(name='OUTPUT_C')


SpeedPercent = Mock(name='SpeedPercent')

MoveTank = Mock(name='MoveTank')

sleeper=Mock(name='sleeper')
attrs = {
        'turn_to_angle': sleep,
        'on_for_distance': sleep
        }
sleeper.configure_mock(**attrs)
MoveDifferential = Mock(name='MoveDifferential', return_value=sleeper)

MoveSteering = Mock(name='MoveSteering')

SpeedRPM = Mock(name='SpeedRPM')

Motor = Mock(name='Motor')

follow_for_ms = Mock('follow_for_ms')
