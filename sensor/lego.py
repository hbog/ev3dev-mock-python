from unittest.mock import Mock

UltrasonicSensor = Mock()

gyro = Mock()
attrs = { 'angle': 5 }
gyro.configure_mock(**attrs)
GyroSensor = Mock(return_value=gyro)

